package project;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;
/**
* A interface specifies the operations on Student object
* @author Ginson
*
*
*/
public class BookServicesImpl implements BookServices {
	Book[]bookArray=new Book[100];
			int count=0;
     /**
     * The method will add a book
     * @param book book to be added
     *
     */

    public void add(Book book) 
     {
    	if(count==100)
    		System.out.println("limit exceeded");
    	else
    		bookArray[count]=book;
    	count++;
     }
    	


    /**
    * The method should display all the books
    */
     public void displayAll() {
     
    	 if(count==100)
    		 System.out.println("no data");
    	 else
    		 for(int i=0;i<count;i++ )
    		 {
    			 bookArray[i].display();
    		 }
     }
     /**
     * The method will search books and display the book with given name
     * @param name name of the book to be searched
     *
     */
     public void search(String name) 
     {	
		for(int i=0;i<count;i++)
		{
			if(bookArray[i].getName().equals(name))
				bookArray[i].display();
		}
    	 
     }
     /**
     * The method will search books and display the book with publish date
     * @param publish date
     *
     */
     public void search1(LocalDate date)
     {
    	
    	 for(int i=0;i<count;i++)
    	 {
			if(bookArray[i].getPublishdate().isAfter(date))
    			// System.out.println(book2[i]);
    	 bookArray[i].display();
    	 } 
     }
   
     /**
     * The method sorts the book based on price
     */
	
	 public  void sort() {
		// TODO Auto-generated method stub
		Arrays.sort(bookArray,0,count);
		
		
			displayAll();
		
	 }
	 /*This method download the book details as CSV file*/
	 
		public void download() {
				getCSVString(bookArray,count);
	}
		
}



