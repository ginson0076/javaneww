package project;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;

public interface BookServices {
	
	void add (Book book);
	
	void displayAll();
	
	void search(String name);
	
	void search1(LocalDate pubdate);
	
	void sort();
	
	void download();
	
	default void getCSVString(Book book[],int count) {
		String data;
		for(int i=0;i<count;i++)
		{
			data=new StringBuffer().append(book[i].getName()).append(",").append(book[i].getPages()).append(",").append(book[i].getPrice()).append(", ").append(book[i].getType()).append(", ").append(book[i].getPublishdate()).append("\n").toString();
		
		try {
			FileWriter file=new FileWriter("BookDetails.csv");
			file.write(data);
			file.close();
			
		}
		catch (IOException exception) {
			exception.printStackTrace();
		}
	}
}
}