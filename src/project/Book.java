package project;

import java.time.LocalDate;
import java.util.Scanner;

public class Book implements Comparable<Book>
{
private	String name;
private	int pages;
private static	int price;
private Type type;
	LocalDate publishdate;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public int getPrice() {
		return price;
	}
	public LocalDate getPublishdate() {
		return publishdate;
	}
	public void setPublishdate(LocalDate pubdate) {
		this.publishdate = pubdate;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public Book(String name,int pages,int price,Type type,LocalDate pubdate)
	{
		this.name=name;
		this.pages=pages;
		this.price=price;
		this.publishdate=pubdate;
		this.type=type;
	}
	public Book() {
}
		public String toString()
		{
			
			{
			return new StringBuffer().append("NAME:").append("name").append("\n").append("PAGES").append("pages").append("\n").append("PRICE:").append("price").append("\n").append("TYPE").append("type").append(",").append("published date:").append(publishdate).append("\n").toString();




			}
		}
		public boolean equals(Book res) {
		if(name.equals(res.name)) {
			return true;
		}
		else 
			return false;
	}
	public void update()
	{
		Scanner scan=new Scanner(System.in);
		System.out.println("enter the name");
		name=scan.next();
		System.out.println("enter the pages");
		pages=scan.nextInt(); 
		System.out.println("enter the price");
		price=scan.nextInt();
		System.out.println("enter the type");
		String type=scan.next();
		System.out.println("enter the published date");
		String date=scan.next();
	    publishdate=LocalDate.parse(date);  
	}
	public void display()
	{
		System.out.println("enter the name  :"+name);
		System.out.println("enter the pages :"+pages);
		System.out.println("enter the price :"+price);
		System.out.println("enter the type  :"+type);
		System.out.println("enter the published date:"+publishdate);
		
		
		
	}
	@Override
	public int compareTo(Book o) {
		// TODO Auto-generated method stub
		if(price<Book.price)
			return -1;
		else if(price>Book.price)
			return 1;
		else
			return 0;
		
	}
	
	
}

	
		

	


